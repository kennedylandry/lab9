import React, { useState } from "react";

function App() {
  const [input, setInput] = useState("");
  const [result, setResult] = useState("");

  const handleInput = (event) => {
    setInput(event.target.value);
  };

  const handlePositiveNumbers = () => {
    const numbers = input.split(",").map(Number);
    const positiveNumbers = numbers.filter((num) => num > 0);
    const sumOfPositiveNumbers = positiveNumbers.reduce((total, num) => total + num, 0);
    setResult(`The sum of positive numbers is: ${sumOfPositiveNumbers}`);
  };

  const handleStatistics = () => {
    const numbers = input.split(",").map(Number);
    const sortedNumbers = numbers.sort((a, b) => a - b);
    const length = sortedNumbers.length;
    const sum = sortedNumbers.reduce((total, num) => total + num, 0);
    const mean = sum / length;
    const median =
      length % 2 === 0
        ? (sortedNumbers[length / 2 - 1] + sortedNumbers[length / 2]) / 2
        : sortedNumbers[Math.floor(length / 2)];
    setResult(`Mean: ${mean.toFixed(1)}\nMedian: ${median}`);
  };

  return (
    <div style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
      <label htmlFor="input" style={{ marginBottom: "1rem" }}>
        Enter a list of numbers separated by commas:
      </label>
      <input
        type="text"
        id="input"
        value={input}
        onChange={handleInput}
        style={{ padding: "0.5rem", borderRadius: "4px", border: "1px solid #ccc", marginBottom: "1rem" }}
      />

      <div style={{ display: "flex", justifyContent: "space-evenly", width: "100%", marginBottom: "1rem" }}>
        <button onClick={handlePositiveNumbers} style={{ padding: "0.5rem", borderRadius: "4px", border: "none", backgroundColor: "#007bff", color: "#fff" }}>
          Calculate Sum of Positive Numbers
        </button>
        <button onClick={handleStatistics} style={{ padding: "0.5rem", borderRadius: "4px", border: "none", backgroundColor: "#007bff", color: "#fff" }}>
          Calculate Mean and Median
        </button>
      </div>

      <p style={{ whiteSpace: "pre" }}>{result}</p>
    </div>
  );
}

export default App;